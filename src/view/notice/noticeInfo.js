import React, {Component} from 'react';
import {Header} from "../common";
import {formatDate} from "../../lib/tool";

export default class App extends Component {
    constructor(props) {
        super(props)
        this.info = this.props.location.state;

    }

    render() {
        return (
            <div className={'noticeBox'}>
                <Header title={'公告'} {...this.props}/>
                <div className={'main'}>
                    <div>
                        <div className={'title'}>{this.info.title}</div>
                        <div className={'time'}>{formatDate('y-m-d h:i:s', {date: this.info.time.time})}</div>
                    </div>
                    <div className={'infoMain'} dangerouslySetInnerHTML={{__html:this.info.content}}/>
                </div>
            </div>
        )
    }
}