import React, {Component} from 'react';
import {Header} from "../common";
import {formatDate} from "../../lib/tool";

export default class App extends Component {
     info= null;
     simulate=null
    constructor(props) {
        super(props)
        this.info  =this.props.location.state.info;
    }

    render() {
        return (
            <div className={'listInfo'}>
                <Header title={'订单记录'} {...this.props}/>
                <div className={'main'}>
                    <div className={'title'}>
                        <div>结算盈亏</div>
                        <div className={this.info.income > 0 ? 'money red':'money green'}>{this.info.income > 0 ? `+${this.info.income}`:this.info.income}<span>元</span></div>
                    </div>
                    <div className={'info'}>
                        <h4>合约信息</h4>
                        <div>
                            <p>交易品种</p>
                            <div>{this.info.contCode}</div>
                        </div>
                        <div>
                            <p>交易数量</p>
                            <div>{this.info.cpVolume}手</div>
                        </div>
                        <div>
                            <p>交易方向</p>
                            <div>{this.info.isBuy ?'多':'空'}</div>
                        </div>
                        <div>
                            <p>保证金</p>
                            <div>{Math.abs(this.info.stopLoss)}元</div>
                        </div>
                        <div>
                            <p>交易综合费</p>
                            <div>0元</div>
                        </div>
                        <div>
                            <p>交易模式</p>
                            <div>{this.info.moneyType === 0? '标准':'迷你'}</div>
                        </div>
                    </div>
                    <div className={'info'}>
                        <h4>合约信息</h4>
                        <div>
                            <p>开仓均价</p>
                            <div>{this.info.opPrice}</div>
                        </div>
                        <div>
                            <p>平仓均价</p>
                            <div>{this.info.cpPrice}</div>
                        </div>
                        {/*<div>*/}
                            {/*<p>平仓类型</p>*/}
                            {/*<div>CL1805</div>*/}
                        {/*</div>*/}
                        <div>
                            <p>开仓时间</p>
                            <div>{formatDate('y-m-d h:i:s',{date:this.info.time.time})}</div>
                        </div>
                        <div>
                            <p>平仓时间</p>
                            <div>{formatDate('y-m-d h:i:s',{date:this.info.tradeTime.time})}</div>
                        </div>
                        <div>
                            <p>订单编号</p>
                            <div>{this.info.id}</div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}