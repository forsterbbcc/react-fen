import React, {Component} from 'react';

import AlertFunction from "../../lib/AlertFunction";
import {Req} from "../../lib";
import {formatDate} from "../../lib/tool";

export default class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            list:null
        }
    }

    render() {
        return (
            <div className={'warp'}>
                {
                    this.state.list === null?'':(
                        this.state.list.length === 0?(
                            <div className={'imgWrap'}>

                            </div>
                        ):(
                            <ul className={'flat'}>
                                {
                                    this.state.list.map((e)=>{
                                        return(
                                            <li>
                                                <table>
                                                    <tbody>
                                                    <tr>
                                                        <th className={'contract'}>
                                                            {e.commodity} ({e.contract})
                                                        </th>
                                                        <th>
                                                            ID{e.id}
                                                        </th>
                                                        <th className={'last'}>
                                                            {formatDate('m-d h:i:s',{date:e.tradeTime.time})}
                                                        </th>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                                <table className={'sub'}>
                                                    <tbody>
                                                    <tr>
                                                        <td className={e.isBuy?'raise':'fall'}>
                                                            买{e.isBuy?'涨':'跌'}{e.volume}手
                                                        </td>
                                                        <td rowSpan={3}>
                                                            买入 0 元
                                                        </td>
                                                        <td rowSpan={3}>
                                                            <div className={'status'}>
                                                                买入失败
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            止盈&nbsp;{e.stopProfit}元
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            止损&nbsp;{e.stopLoss}元
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </li>
                                        )
                                    })
                                }
                            </ul>
                        )
                    )
                }
            </div>
        )
    }

    componentDidMount(){
        this.mounted = true;
        this.updateStore();
    }

    componentWillUnmount(){
        this.mounted = false;
    }

    async updateStore() {
        try {
            let {data} = await Req({
                url: '/api/trade/scheme.htm',
                data: {
                    schemeSort: 3,
                    tradeType: this.props.location.state.simulate?2:1,
                    beginTime: '',
                    _: new Date().getTime()
                },
                animate:true
            });
            if(this.mounted){
                this.setState({list: data});
            }
        } catch (err) {
            AlertFunction({title:'错误',msg:err['errorMsg'] || err});
        }
    }
}