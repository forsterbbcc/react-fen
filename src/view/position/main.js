import React,{Component} from 'react'
import{NavLink}from 'react-router-dom'
import RouteWithSubRoutes from '../../routes/routeWithSubRoutes';
import { Header, Alert, Svg } from '../common';
import Position from './../position/index';
import End from './../position/settlement';
import { Schedule, Req } from '../../lib';
import { Data, Contracts, Cache } from '../../module';
import AlertFunction from '../../lib/AlertFunction';
import LoadingFunction from '../../lib/loading';

import back from './../../images/qh/back.svg'

export default class App extends Component{
    constructor(props){
        super(props);

        this.state = {
            page:this.props.location.state.page || 'position'
        }
    }

    componentDidMount(){
        this.mounted = true;
        if (Contracts.initial) {
            Data.start('position');
        } else {
            Schedule.addEventListener('contractsInitial', () => {
                Data.start('position');
            }, this)
        }

        if (!!Cache.tradeList) {
            this._keepUpdate = true;
            this.updatePosition(true);
            
        } else {
            Schedule.addEventListener('getSchemeInfo', () => {
                this._keepUpdate = true;
                this.updatePosition(true);
            }, this)
        }
        this.updateStore();
    }

    componentWillUnmount(){
        Data.end('position');
        Schedule.removeEventListeners(this);
        clearTimeout(this._keepUpdate);
        this._keepUpdate = null;
    }

    async updateStore() {
        try {
            let {data} = await Req({
                url: '/api/trade/scheme.htm',
                data: {
                    schemeSort: 2,
                    tradeType: 2,//this.props.state.simulate ? 2 : 1,
                    beginTime: '',
                    _: new Date().getTime()
                },
                animate: true
            });
            let sum = 0;
            for (let i = 0; i < data.length; i++) {
                const element = data[i].volume;
                sum = sum + element;
            }
            this.setState({
                sum:sum
            });
        } catch (err) {
            AlertFunction({title: '错误', msg: err['errorMsg'] || err});
        }
    }

    renderHeaderComponent(){
        if (this.state.page === 'position') {
            return(
                <div className={'headerComponentWrapper'}>
                    <div>
                        <div className={'des'}>浮动盈亏&nbsp;&nbsp;</div>
                    </div>
                    <div className={'headerBottomWrapper'}>
                        <div className={'money'}>{this.state.income >= 0 ? `+${this.state.income }` : this.state.income}元<span></span></div>
                        <div className={'btn'} onClick={() => this.closeAll()}>一键清仓</div>
                    </div>
                </div>
            );
        } else {
            return(
                <div className={'headerComponentWrapper'}>
                    <div>
                        <div className={'des'}>总交易手数&nbsp;&nbsp;</div>
                    </div>
                    <div className={'headerBottomWrapper'}>
                        <div className={'money'}>{this.state.sum}手<span></span></div>
                        <div className={'btn'} onClick={() => this.addSimBalance()}>{'一键加币'}</div>
                    </div>
                </div>
            );
        }
    }

    renderContent(){
        if (this.state.page === 'position') {
            return(
                <Position/>
            );
        } else {
            return(
                <End list={this.state.list} state={{simulate:true, type:1}}/>
            );
        }
    }

    render(){
        let positionStyle = this.state.page === 'position' ? ' pageSelected':'';
        let endStyle = this.state.page === 'end' ? ' pageSelected':'';
        
        return(
            <div className={'positionMain'}>
                {/* <Header title={'持仓'} {...this.props}/> */}
                <div className={'p-header'}>
                    <div onClick={()=>window.history.back()} className={'imageBox'}>
                        <Svg path={back}/>
                    </div>
                    <div className={'pageSelector'}>
                        <div onClick={()=>this.setState({page:'position'})} className={'title rightLine '+positionStyle}>持仓</div>
                        <div onClick={()=>this.setState({page:'end'})} className={'title '+endStyle}>结算</div>
                    </div>
                </div>

                {/* <div className={'topButtons'}>
                    <div onClick={()=>this.goPage('position')} className={'topButton'+positionStyle}>持仓列表</div>
                    <div onClick={()=>this.goPage('end')} className={'topButton'+endStyle}>结算列表</div>
                </div> */}
                {this.renderContent()}
            </div>
        );
    }

    goPage(value){
        this.setState({
            page:value
        });
    }

    /* 更新持仓
    */
   async updatePosition(init) {
       try {
           let {data} = await Req({
               url: '/api/trade/scheme.htm',
               data: {
                   schemeSort: 1,
                   tradeType: 2,
                   beginTime: '',
                   _: new Date().getTime(),
               },
               animate: init
           });
           if (data) {
               this.dealPosition(data);
           }
       } catch (err) {
            AlertFunction({title: '错误', msg: err['errorMsg'] || err});
       } finally {
           if (this._keepUpdate !== null) {
               this._keepUpdate = setTimeout(() => this.updatePosition(), 1000);
           }
       }
   }

   /**
    * 更新持仓数据
    */
   dealPosition(data) {
       
       let quote, scheme;
       let income = 0;
       let position = data.map((e) => {
           quote = Data.total[e.contract];
           scheme = Cache.tradeList[e.contract];

           if (quote) {
               e.unit = scheme.priceUnit.mul(scheme.priceChange).mul(e.moneyType === 0 ? 1 : 0.1);
               e.current = Number(quote.price) || 0;
               if (!!quote.price && !!e.opPrice) {
                   if (e.isBuy) {
                       e.income = e.current.sub(e.opPrice).mul(e.volume).mul(scheme.priceUnit).mul(e.moneyType === 0 ? 1 : 0.1);
                   } else {
                       e.income = e.opPrice.sub(e.current).mul(e.volume).mul(scheme.priceUnit).mul(e.moneyType === 0 ? 1 : 0.1);
                   }
                   income = income.add(e.income);
                   return e;
               } else {
                   e.income = 0;
                   return e;
               }
           } else {
               return null;
           }
       });
       position.remove(null);
       if (position.findIndex((e) => e === null) === -1) {
           if (this.mounted) {
               this.setState({list: position, income: income});
           }
       }
   }

   async closeAll() {
        try {
            let t = this.state.list.map((o) => {
                return Req({
                    url: '/api/trade/close.htm',
                    type: 'POST',
                    data: {
                        bettingId: o.id,
                        tradeType: 2 ,
                        source: '下单'
                    },
                    ignore: true
                });
            });
            LoadingFunction();
            Promise.all(t).then((o) => {
                Schedule.dispatchEvent({event: 'loadingEnd'});
                let success = 0;
                let failure = 0;
                o.forEach((e) => {
                    if (e.success) {
                        success++;
                    } else {
                        failure++;
                    }
                });
                AlertFunction({title: '提示', msg: `平仓委托已提交,成功${success}单,失败${failure}单`});
                Cache.getUserInfo();
            });
        } catch (err) {
            AlertFunction({title: '错误', msg: err.errorMsg || err});
        }
    }

    async addSimBalance() {
        
        try {
            const result = await Req({
                url: '/trade/addScore.htm',
                animate: true
            });
            Cache.getUserInfo();
            AlertFunction({title: '提示',msg:result.resultMsg});
            
        } catch (err) {
            AlertFunction({title: '提示',msg:err.resultMsg});
        }
    }

}

