import React, {Component} from 'react';
import empty from '../../images/Nf-svg/empty.svg';
import {Req} from "../../lib";
import AlertFunction from "../../lib/AlertFunction";
import {formatDate} from "../../lib/tool";
import {Header} from "../common";
import {Svg} from "../common";
import {Link} from 'react-router-dom';

import i from '../../images/Nf-svg/i.svg'
import {Data} from "../../module";

export default class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            list: null
        }
    }

    render() {
        return (
            <div className={'position settlement'}>
                {/* <Header title={this.props.location.state.simulate ? '结算列表(模拟)' : '结算列表'} {...this.props}/> */}
                {/* <Header title={this.props.state.simulate ? '结算列表(模拟)' : '结算列表'} {...this.props}/> */}
                <div className={'warp'}>
                    {
                        this.state.list === null ? '' : (
                            this.state.list.length === 0 ? (
                                <div className={'imgWrap'}>
                                    <Svg path={empty} className={'empty'}/>
                                    <p>暂时没有数据</p>
                                </div>
                            ) : (
                                <ul className={'flat'}>
                                    {
                                        this.state.list.map((item) => {
                                            return (
                                                <li className={item.income >= 0 ? 'raiseLine settlementLI' : 'fallLine settlementLI'}>
                                                    <Link className={'cell'} to={{
                                                        pathname: '/listInfo',
                                                        state: {simulate: 2, info: item}
                                                    }}>
                                                        <div>
                                                            <div>
                                                                <div className={'commodityWrapper'}>
                                                                    <div className={'commodity'}>{item.commodity} ({item.contract})</div>
                                                                </div>
                                                                {/* <div className={'normalTextColor'}>{formatDate('y年m月d日',{date:item.tradeTime.time})}</div> */}
                                                            </div>
                                                            <table border="1">
                                                                <tbody>
                                                                <tr>
                                                                    <td>止损 {item.stopLoss}</td>
                                                                    <td>买入 {item.opPrice.toFixed(item.priceDigit)}</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>止盈 {item.stopProfit}</td>
                                                                    <td>买出 {item.cpPrice.toFixed(item.priceDigit)}</td>
                                                                </tr>
                                                                {/* <tr>
                                                                    <td className={'normalTextColor'}>已平仓</td>
                                                                    <td className={item.isBuy ? 'raise' : 'fall'}> {item.income > 0 ? `+${item.income}` : item.income} </td>
                                                                    <td></td>
                                                                </tr> */}
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <div className={'statusBox'}>
                                                            <div className={'normalTextColor'}>已平仓</div>
                                                            <div className={item.isBuy ? 'raise' : 'fall'}> {item.income > 0 ? `+${item.income}` : item.income} </div>
                                                        </div>
                                                    </Link>
                                                </li>
                                            )
                                        })
                                    }
                                </ul>
                            )
                        )
                    }
                </div>
            </div>
        )
    }

    componentDidMount() {
        this.mounted = true;
        this.updateStore();
    }

    componentWillUnmount() {
        this.mounted = false;
    }

    async updateStore() {
        try {
            let {data} = await Req({
                url: '/api/trade/scheme.htm',
                data: {
                    schemeSort: 2,
                    tradeType: 2,//this.props.state.simulate ? 2 : 1,
                    beginTime: '',
                    _: new Date().getTime()
                },
                animate: true
            });
            if (this.mounted) {
                this.setState({list: data});
            }
        } catch (err) {
            AlertFunction({title: '错误', msg: err['errorMsg'] || err});
        }
    }
}