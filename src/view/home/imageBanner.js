import React, {Component} from 'react';
import ReactSwipe from 'react-swipe';
import {Link} from 'react-router-dom';
import {Store} from "../../module/index";

import banner1 from './../../images/b1.png';
import banner2 from './../../images/b2.png';
import banner3 from './../../images/b3.png';

export default class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            banners: [banner1,banner2,banner3]
        }
    }

    render() {
        return (
            <div className={'bannerBox'}>
                <ReactSwipe swipeOptions={{continuous: true, speed: 1000, startSlide: 0, auto: 3000}}
                            key={this.state.banners.length}>
                            
                            {
                                this.state.banners.map((image,i)=>{
                                    return(
                                        <a>
                                            <img className={'img'}
                                                src={image}
                                                alt={''}/>
                                        </a>
                                    );
                                })
                            }
                                
                    {/* {
                        this.state.banners.map((item) => {
                            if (item.key.indexOf('/') === -1) {
                                return (
                                    <a>
                                        <img className={'img'}
                                            src={process.env.NODE_ENV === 'development' ? `https://rx.hot7h.com/${item.url}` : item.url}
                                            alt={item.mcname}/>
                                    </a>
                                )
                            } else {
                                return (
                                    <Link to={item.key}>
                                        <img className={'img'}
                                            src={process.env.NODE_ENV === 'development' ? `https://rx.hot7h.com${item.url}` : item.url}
                                            alt={item.mcname}/>
                                    </Link>
                                )
                            }

                        })
                    } */}
                </ReactSwipe>
            </div>
        )
    }

    componentDidMount() {
        this.mounted = true;
        // Store.banner.get().then((data) => {
        //     if (this.mounted) {
        //         this.setState({
        //             banners: data
        //         })
        //     }
        // })
    }

    componentWillUnmount() {
        this.mounted = false;
    }
}
