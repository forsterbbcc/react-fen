import React, {Component} from 'react';
import ReactSwipe from 'react-swipe';
import {Link} from 'react-router-dom'
import {Store} from "../../module";

export default class App extends Component {
    constructor(props) {
        super(props);
        this.state={
            notices:[]
        }
    }

    render() {
        return (
            <div className={'homeNoticeBox'}>
                <div className={'title'}>公告</div>
                <ReactSwipe className="noticesBox"
                            swipeOptions={{continuous: true, speed: 1000, startSlide: 0, auto: 2000}}
                            key={this.state.notices.length}>
                    {
                        this.state.notices.map((item) => {
                            return (
                                <Link to={'/notice'}>{item.title}</Link>
                            )
                        })
                    }
                </ReactSwipe>
            </div>
        )
    }

    componentDidMount(){
        this.mounted = true;
        Store.homeNotices.get().then((data)=>{
            if(this.mounted){
                this.setState({
                    notices:data
                })
            }
        })
    }
    componentWillUnmount(){
        this.mounted = false;
    }
}