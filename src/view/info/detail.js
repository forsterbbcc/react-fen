import React, {Component} from 'react'
import Header from '../common/header'
import {Req} from '../../lib'

export default class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            content:null
        }

        if (this.props.location.state) {
            let ary = JSON.parse(localStorage.getItem('record')) || [];
            let currentItem = this.props.location.state;
            let result = ary.find(((element)=>{
                return element.id === currentItem.id;
            }))
            if (result) {
                // ary.splice(item, 1);
            }else{
                ary.push(currentItem)
            
            }
            let Ary = JSON.stringify(ary);
            localStorage.setItem('record', Ary);
        }
    }

    render() {
        if (!!this.state.content) {
            return (
                <div className={'infodetail'}>
                    <Header title={'资讯详情'} {...this.props}/>
                    <div>
                        <h2>{this.state.title}</h2>
                        <p>{this.state.date}</p>
                    </div>
                    <div dangerouslySetInnerHTML={{__html:this.state.content}}></div>
                </div>
            );
        }else{
            return (
                // <div className={'infodetail'}>
                //     <header>资讯详情</header>
                // </div>
                ''
            );
        }
    }

    componentDidMount(){
        let id = this.props.match.params.id;
        this.getDetail(id);
    }

    async getDetail(id) {
        try {
          const result = await Req({
            url: '/api/news/newsDetail.htm',
            data: {
              id: id
            },
              animate:true
          })
          this.setState({
              content:result.news.content,
              date:result.news.date,
              title:result.news.title
          });
          //console.log(result);
        } catch (err) {
          //console.log(err);
        }
      }
}