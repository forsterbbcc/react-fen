import React,{Component} from 'react'
import {NavLink} from 'react-router-dom'
import RouteWithSubRoutes from '../../routes/routeWithSubRoutes';
import { Header } from '../common';
import { completeNum } from '../../lib/tool';

export default class App extends Component {

    constructor(props){
        super(props);

        this.state = {
            page:'event',
            show:this.diff(0).show,
            date:this.diff(0).val
        }
    }

    componentWillMount(){

    }

    renderDate(){
        let arr = this.getWeekTime();
        return arr.map((item,i)=>{
            return(
                <li key={i}>
                    <div className={this.state.show === item.show?'active':''} onClick={()=> this.setState({date:item.val,show:item.show})}>{item.show}</div>
                </li>
            );
        });
    }

    render(){
        console.log(this.state.date,'this.state.date')
        const a = this.props.routes;
        if (this.state.show) {
            return(
                <div className={'financeDataBox'}>
                    <Header title={'财经'} {...this.props} back={'/'}/>
                    <div className={'categoryBox'}>
                        <NavLink to={{pathname:'/finance/event',state:{show:this.state.show}}} className={`categoryButton`} activeClassName={'active'}>财经事件</NavLink>
                        {/* <NavLink to={'/finance/holiday'} className={`categoryButton`} activeClassName={'active'}>财经假期</NavLink> */}
                    </div>
                    <ul className={'dateList'}>
                        {this.renderDate()}
                    </ul>
                    {a.map((route, i) => <RouteWithSubRoutes date={this.state.date} key={i} {...route} />)}
                </div>
            );
        }
    }

    diff(v) {
        let now = new Date();
        let date = new Date(now.getTime() + v * 24 * 3600 * 1000);
        let year = date.getFullYear();
        let month = date.getMonth() + 1;
        month = completeNum(month);
        let day = date.getDate();
        day = completeNum(day);
        //todo 计算当前月的时常

        let obj = {};
        obj.show = day;
        obj.val = year.toString() + month.toString() + day.toString();
        let week = ["日", "一", "二", "三", "四", "五", "六"];
        obj.week = '星期' + week[date.getDay()];
        obj.today = v === 0;
        obj.monthLength = new Date(year, month, 0).getDate();
        return obj;
    }

    //todo 获取一周内数据
    getWeekTime(){
        let ary = [], num = -7;
        for(let i =0 ;i < 7; i++){
            num ++;
            ary.push(this.diff(num))
        }
        return ary
    }
}