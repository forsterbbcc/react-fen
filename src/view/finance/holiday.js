import React,{Component} from 'react'
import { Req } from '../../lib';

export default class App extends Component {

    constructor(props){
        super(props);

        this.state = {
            newsHoliday:[]
        }
    }

    componentDidMount(){
        this.update(this.props.date)
    }

    componentWillReceiveProps(nextProps){
        if (this.props !== nextProps) {
            // console.log(this.props,nextProps,'nextProps')
            this.update(nextProps.date)
        }
    }

    renderItem(){
        return this.state.newsHoliday.map((item,i)=>{
            return(
                <li key={i} className={'cell'}>
                    <div className={'title'}>{item.exchangename}因{item.holidayname+item.note}</div>
                    <div className={'date'}>{item.date}</div>
                </li>
            );
        })
    }

    render(){
        if (this.state.newsHoliday.length === 0) {
            return(
                <div className={'holidayBox'}>
                    <div className={'emptyBox'}>
                        <div>暂无数据</div>
                    </div>
                </div>
            );
        } else {
            return(
                <div className={'holidayBox'}>
                    <ul className={'holidayList'}>
                        {this.renderItem()}
                    </ul>
                </div>
            );
        }
    }

    async update(date){
        // console.log(date,'date')
        try {
            let result = await Req({
                url:`/api/news/calendar.htm`,
                data:{
                    date:date
                },
                animate:!this.props.small
            })

            this.setState({
                newsHoliday:result.news.newsHoliday,
            });
            // console.log(result.news.newsHoliday);
        } catch (error) {
            
        }
    }
}