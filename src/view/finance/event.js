import React,{Component} from 'react'
import { Req } from '../../lib';

export default class App extends Component {

    constructor(props){
        super(props);
        this.state = {
            newsEvent:[]
        }
    }

    componentDidMount(){
        console.log(this.props,'0')
        this.update(this.props.date)
    }

    componentWillReceiveProps(nextProps){
        if (this.props !== nextProps) {
            this.update(nextProps.date)
        }
    }

    renderItem(){
        return this.state.newsEvent.map((item,i)=>{
            return (
                <li className={'cell'} key={i}>
                    <div className={'location'}>{`${(item.city===null||item.city==='')?' ,':''} ${item.country}`}</div>
                    <div className={'content'}>{item.eventcontent}</div>
                    <div className={'date'}>{item.datetime}</div>
                </li>
            );
        });
    }

    render(){
        
        if (this.state.newsEvent.length === 0) {
            return (
                <div className={'eventBox'}>
                    <div className={'emptyBox'}>
                        <div>暂无数据</div>
                    </div>
                </div>
            );
        }else{
            return(
                <div className={'eventBox'}>
                    <ul className={'eventList'}>
                        {this.renderItem()}
                    </ul>
                </div>
            );
        }
    }

    async update(date){
        console.log(date,'date')
        try {
            let result = await Req({
                url:`/api/news/calendar.htm`,
                data:{
                    date:date
                },
                animate:!this.props.small
            })

            this.setState({
                newsEvent:result.news.newsEvent,
            });
        } catch (error) {
            
        }
    }
}