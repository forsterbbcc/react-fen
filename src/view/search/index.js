import React,{Component} from 'react'
import {Link} from 'react-router-dom'
import { Contracts } from '../../module';
import { Schedule } from '../../lib';

export default class App extends Component {
    constructor(props){
        super(props);

        this.state = {
            searchShow:false,
            searchInfo:''
        }
    }

    componentDidMount(){
        Schedule.addEventListener('openSearch',this.show,this);
    }

    componentWillUnmount(){
        Schedule.removeEventListener(this);
    }

    show(){
        this.setState({
            searchShow:!this.state.searchShow
        });
    }

    render(){

        if (!this.state.searchShow) {
            return '';
        }

        return(
            <div className={'searchArea'}>
                <div className={'inputBox'}>
                    <input type={'text'} value={this.state.searchInfo}  placeholder={'请输入商品名称\/合约号等信息'}
                        maxLength={20}
                        onChange={e=>{
                            this.setState({searchInfo:e.currentTarget.value});
                            this.search(e.currentTarget.value);
                    }}/>
                    <div class={'cancelButton'} onClick={()=>this.setState({searchShow:false})}>取消</div>
                </div>
                <div>
                    {
                        this.search(this.state.searchInfo).map((item) => {
                            return (
                                <div onClick={()=>this.setState({searchShow:false})}>
                                    <Link
                                        to={{
                                            pathname: '/trade',
                                            state: {
                                                simulate: true,
                                                contract: item.contract,
                                                code: item.contract,
                                                name:item.name,
                                                goodsCode: item.code
                                            }
                                        }}>{item.name}
                                    </Link>
                                </div>
                            )
                        })
                    }
                 </div>
            </div>
        );
    }

     //todo 搜索
     search(info) {
        if (!info) {
            return [];
        } else {
            //todo 如果以字母与数字搜索
        if((/^[A-Za-z0-9]+$/).test(info)){
            info = info.toUpperCase();
            return Contracts.totalArray.filter((item) => {
                return item.contract.startsWith(info)
            });
        }else if(/^[\u4e00-\u9fa5]{0,}$/){ //todo 如果以汉字搜索
            return Contracts.totalArray.filter((item) => {
                return item.name.startsWith(info)
            });
        }
        }
    }
}