import React, {Component} from 'react';
import {Contracts} from "../../module";
import {Schedule} from "../../lib";
import {Svg} from "../common";
import {Link} from 'react-router-dom'

import back from '../../images/qh/back.svg';
import search from '../../images/qh/search.svg';
import downArrow from './../../images/Nf-svg/back.svg'
import favorite from './../../images/qh/favorite.svg'
import remove_favorite from './../../images/qh/remove-favorite.svg'

export default class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            simulate: true,
            name: '获取中',
            code: '获取中',
            active: false
        };

        let c = Contracts.getContract(this);
        if (!!c) {
            this.state.name = Contracts.total[c].name;
            this.state.code = c;
        } else {
            Schedule.addEventListener('contractsInitial', this.updateContracts, this);
        }
        //todo 获取缓存
        if (localStorage.getItem('self') !== null) {
            this.getSelf()
        }

    }

    render() {
        return (
            <nav className={'t-header'}>
                <a className={'backTag'} onClick={()=>window.history.back()}>
                    <Svg path={back} className={'back'}/>
                </a>
                <div className={'group'}>
                    <div className={'cell'}>
                        {/* <div>{this.state.code+'/'+this.state.name} </div> */}
                        <div className={'code'}>{(this.props.location.state.contract||this.state.code)+'/'+(this.props.location.state.name||this.state.name)}</div>
                    </div>
                    <div onClick={this.props.openSelect}>
                        <Svg path={downArrow} className={'arrowDown'}/>
                    </div>
                </div>

                {/* <div className={'rule'} onClick={() => {this.props.active()}}>
                    <Svg path={search}/> 
                </div> */}
                <div className={'iconBox'}>
                    <div className={this.state.active ? 'starActive' : ''}
                         onClick={() => this.chooseStar(this.props.location.state.goodsCode)}>
                        <Svg path={this.state.active?remove_favorite:favorite}/>
                    </div>
                    <div onClick={() => {this.props.active()}}>
                        <Svg path={search}/>             
                    </div>
                </div>

            </nav>
        )
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.location.state.simulate !== nextProps.location.state.simulate) {
            const o = Contracts.total[nextProps.location.state.contract];
            this.setState({
                simulate: nextProps.location.state.simulate,
                name: o.name,
                code: o.contract,
            })
        }
    }

    componentWillUnmount() {
        Schedule.removeEventListeners(this)
    }

    updateContracts() {
        let c = Contracts.getContract(this);
        if (!!c) {
            this.setState({
                name: Contracts.total[c].name,
                code: c
            });
        }
    }

    //todo 选择自己喜欢
    chooseStar(code) {
        let ary = JSON.parse(localStorage.getItem('self')) || [];
        //todo 先检查是否存在，如果存在就移除， 如果不存在就添加
        ary.includes(code) ? ary.remove(code) :  ary.push(code);
        this.setState({active: !this.state.active});
        let Ary = JSON.stringify(ary);
        localStorage.setItem('self', Ary);
        Contracts.updateSelf()
    }

    //todo 获取
    getSelf() {
        let ary = JSON.parse(localStorage.getItem('self'));
        let code = this.props.location.state.goodsCode || this.props.location.state.contract;
        if (ary.includes(code)) {
            this.state.active = true;
        }
    }
}