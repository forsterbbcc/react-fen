import React, {Component} from 'react';
import {Chart, Contracts} from "../../module";
import {Schedule} from "../../lib";

import upArrow from './../../images/mdf-svg/up-arrow.svg'
import downArrow from './../../images/mdf-svg/down-arrow.svg'
import { Svg } from '../common';

export default class App extends Component {
    team = [
        {
            name:'分时',
            key:'sline'
        },
        {
            name:'日线',
            key:'1D'
        },
        {
            name:'1分',
            key:'1'
        },
        {
            name:'3分',
            key:'3'
        },
        {
            name:'5分',
            key:'5'
        },
        {
            name:'15分',
            key:'15'
        },
        // {
        //     name:'盘口',
        //     key:'dynamic'
        // }
    ];

    constructor(props) {
        super(props);
        this.state = {
            select: 'sline',
            price: '',
            rate: '',
            percent: '',
            open: '-',
            close: '-',
            max: '-',
            min: '-',
            maxTend: 'raise',
            minTend: 'fall'
        };

    }

    render() {
        return (
            <div className={'chartBar'}>
                <div className={'chartInfo'}>
                    <div className={`dataWrapper`}>
                        {/* <div className={'rate'}>期货数据</div> */}
                        <div className={this.state.price === '' ? 'hidePriceBox':'priceBox'}>
                            <div className={`${this.state.tend} nowPrice`}>{this.state.price}</div>
                            {/* <div className={'svg'}>
                                <Svg className={`${this.state.tend}Arrow`} path={this.state.tend==='raise'?upArrow:downArrow}/>
                            </div> */}
                            <div className={'rate-and-percent-box'}>
                                <div className={`${this.state.tend} rate`}>{this.state.rate}</div>
                                <div className={`${this.state.tend} percent`}>&nbsp;&nbsp;{`(${this.state.percent})`}</div>
                            </div>
                        </div>
                        {/* <div className={'price'}>
                            <div>
                                <div className={'left'}>开盘 <span className={this.state.minTend}>{this.state.open}</span></div>
                                 <div className={'left'}>昨收 <span className={this.state.minTend}>{this.state.close}</span></div>
                            </div>
                            <div>
                                <div>最高 <span className={this.state.maxTend}>{this.state.max}</span></div>
                                <div>最低 <span className={this.state.maxTend}>{this.state.min}</span></div>
                            </div>
                        </div> */}

                    </div>
                    {/* <div className={'price'}>
                        <div>
                            <div className={'left'}>开盘 <span className={this.state.minTend}>{this.state.open}</span></div>
                            <div className={'left'}>昨收 <span className={this.state.minTend}>{this.state.close}</span></div>
                        </div>
                        <div>
                            <div>最高 <span className={this.state.maxTend}>{this.state.max}</span></div>
                            <div>最低 <span className={this.state.maxTend}>{this.state.min}</span></div>
                        </div>
                    </div> */}
                </div>
                <ul>
                    {
                        this.team.map(({name,key})=>{
                            return(
                                <li>
                                    <div className={this.state.select===key?'active':''} onClick={()=>{this.selectType(key)}}>{name}</div>
                                </li>
                            )
                        })
                    }
                </ul>

            </div>
        )
    }

    componentDidMount() {
        Schedule.addEventListener('quoteUpdate', this.updateQuote, this);
    }

    componentWillUnmount() {
        Schedule.removeEventListeners(this);
    }

    updateQuote(e) {
        const {priceDigit} = Contracts.total[e.code];
        const prev = e.settle_price_yes || e.close;
        let rate = e.price.sub(prev);
        let percent = rate.div(prev);
        let tend = rate >=0?'raise':'fall';
        rate = `${rate >= 0 ? '+' : ''}${rate.toFixed(priceDigit)}`;
        percent = `${rate >= 0 ? '+' : ''}${percent.mul(100).toFixed(2)}%`;
        this.setState({
            price: e.price.toFixed(priceDigit),
            rate: rate,
            percent: percent,
            tend:tend,
            open: e.open ? e.open.toFixed(priceDigit) : '-',
            close: e.close ? e.close.toFixed(priceDigit) : '-',
            max: e.max ? e.max.toFixed(priceDigit) : '-',
            min: e.min ? e.min.toFixed(priceDigit) : '-',
        })
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.location.state.contract !== nextProps.location.state.contract) {
            const o = Contracts.total[nextProps.location.state.contract];

            this.setState({
                name: o.name,
                code: o.contract,
                price: '',
                rate: '',
                percent: ''
            });
        }
    }

    selectType(key){
        this.setState({select:key});
        Chart.swap({type:key});
    }

}