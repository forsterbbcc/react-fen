import React,{Component} from 'react';
import {NavLink} from 'react-router-dom'

import RouteWithSubRoutes from '../../routes/routeWithSubRoutes';
import { Svg } from '../common';

import back from '../../images/Nf-svg/back.svg';
import loginLogo from './../../images/login-logo.png'

export default class App extends Component {
    constructor(props){
        super(props);

        this.state = {

        }
    }

    render(){
        const a = this.props.routes;
        return(
            <div className={'loginMain'}>
                <div className={'header'} onClick={() => window.history.back()}>
                        <Svg path={back}/>
                </div>
                <div className={'loginLogoWrapper'}>
                    <img className={'loginLogo'} src={loginLogo} alt={''}/>
                </div>
                <div className={'linkGroup'}>
                    <div className={'linkWrapper'}>
                        <NavLink to={'/login/account'} activeClassName={'linkActive'}>
                            <div>登录</div>
                        </NavLink>
                    </div>
                    <div className={'linkWrapper'}>
                        <NavLink to={'/login/register'} activeClassName={'linkActive'}>
                            <div>注册</div>
                        </NavLink>
                    </div>
                </div>
                {a.map((route, i) => <RouteWithSubRoutes key={i} {...route} />)}
            </div>
        );
    }
} 