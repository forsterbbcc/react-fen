import React, {Component} from 'react';
import {Svg} from "../common";

import banner from '../../images/Nf-img/liveBanner.png'
import user from '../../images/Nf-img/user.png'
import send from '../../images/Nf-svg/send.svg'
import AlertFunction from "../../lib/AlertFunction";
import { Req } from '../../lib';

export default class App extends Component {
    constructor(props) {
        super(props)
        this.state = {
            lives:[]
        }
    }

    componentDidMount(){
        this.update();
    }

    async update(){
        Req({
            url:'/api/news/expressList.htm',
            data:{
                maxId:0
            }
        }).then(result=>{
            
            // let tempArr = result.newsList;
                let dataArr = []
                for (let i = 0; i < result.newsList.length; i++) {
                    let element = result.newsList[i];
                    let spliceResult = element.split('#');
                    let obj = null;

                    let content = spliceResult[3].replace(new RegExp('<p>','g'),'');
                    content = content.replace(new RegExp('</p>','g'),'');
                    content = content.replace(new RegExp('<b>','g'),'');
                    content = content.replace(new RegExp('</b>','g'),'');
                    content = content.replace(new RegExp('<br/>','g'),'');
                    content = content.replace(new RegExp(' ','g'),'');

                    if (spliceResult.length === 12) {
                        obj = {
                            origin: element,
                            date: spliceResult[2],
                            content: content,
                            id: spliceResult[spliceResult.length - 1]
                        }
                        dataArr.push(obj);
                    } else if (spliceResult.length === 14) {
                        obj = {
                            origin: element,
                            date: spliceResult[8],
                            content: content,
                            id: spliceResult[spliceResult.length - 2],
                            qz: '前值：' + spliceResult[3],
                            yq: '预期：' + spliceResult[4],
                            sj: '实际：' + spliceResult[5],
                            tag: spliceResult[7],
                            star: spliceResult[6],
                            country: 'https://res.6006.com/jin10/flag/' + spliceResult[9].substr(0, 2) + '.png'
                        }
                    }
                    
                }

                this.setState({
                    lives:dataArr
                });
        }).catch(error=>{
            console.log(error)
        });
    }

    renderItem(){
        return this.state.lives.map((item,index)=>{
            return(
                <tr key={index}>
                    <td>
                        <div className={'contentWrapper'}>
                            <p>{item.title}</p>
                            <div className={'listContent'}>{item.content}</div>
                            <div className={'listDate'}>{item.date}</div>
                        </div>
                    </td>
                </tr>
            );
        });
    }

    render() {
        return (
            <div className={'live'}>
                <table>
                    <tbody>
                        {this.renderItem()}
                    </tbody>
                </table>
            </div>
        )
    }

}