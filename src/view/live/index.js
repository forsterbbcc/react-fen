import React, {Component} from 'react';
import {NavLink,Link} from 'react-router-dom';
import {Header, Svg} from "../common";
import RouteWithSubRoutes from '../../routes/routeWithSubRoutes';

import back from './../../images/qh/back.svg'

export default class App extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        const a = this.props.routes;
        return (
            <div className={'liveBox'}>
                {/* <Header title={'资讯中心'} back={'/'}/>
                <div className={'main'}>
                    <div>
                        <NavLink to={'/live/finance'} activeClassName={'selected'}>原油</NavLink>
                        <NavLink to={'/live/lives'} activeClassName={'selected'}>7x24</NavLink>
                        <NavLink to={'/live/notice'} activeClassName={'selected'}>公告</NavLink>
                        <NavLink to={'/live/news'} activeClassName={'selected'}>金银</NavLink>
                    </div>
                </div> */}
                <div className={'i-header'}>
                    <Link to={'/'} className={'imageBox'}>
                        <Svg path={back}/>
                    </Link>
                    <div>
                        <div className={'pageSelector'}>
                            <NavLink to={'/live/lives'} activeClassName={'pageSelector-select'}>直播</NavLink>
                            <NavLink to={'/live/finance'} activeClassName={'pageSelector-select'}>原油</NavLink>
                            {/* <NavLink to={'/live/news'} activeClassName={'pageSelector-select'}>金银</NavLink> */}
                        </div>
                    </div>
                </div>
                {a.map((route, i) => <RouteWithSubRoutes key={i} {...route} />)}
            </div>
        )
    }
    componentDidMount(){

    }

}