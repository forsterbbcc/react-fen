import React, {Component} from 'react'
import ReactPullLoad, {STATS} from 'react-pullload'
import {Req} from '../../lib'
import {Link} from 'react-router-dom';
//星星图片
import oneStar from './../../images/1.png'
import twoStar from './../../images/2.png'
import threeStar from './../../images/3.png'
import fourStar from './../../images/4.png'
import fiveStar from './../../images/5.png'
import love from './../../images/xlt/love.svg'
import { Svg } from '../common';
import AlertFunction from '../../lib/AlertFunction';

export default class App extends Component {

    mount = true

    constructor(props) {
        super(props);
        this.state = {
            hasMore: true,
            data: [],
            action: STATS.init,
            date: {
                month:'',
                day:''
            }
        }
    }

    render() {
        return (
            <div className={'news'}>
                <ReactPullLoad
                    downEnough={50}
                    action={this.state.action}
                    handleAction={this.handleAction}
                    hasMore={true}
                    style={{
                        backgroundColor: "#182535",
                    }}
                    distanceBottom={1000}>
                    <ul>
                        {this.renderItem()}
                    </ul>
                </ReactPullLoad>
            </div>
        );
    }

    componentDidMount() {
        this.handRefreshing();
    }

    componentWillUnmount(){
        this.mount = false
    }

    async update(type, date) {
        try {
            const result = await Req({
                url: '/api/news/newsList.htm',
                data: {
                    type: type,
                    date: date
                }
            })
            if (this.mount === false) {
                return;
            }
            if (!date) {
                if (STATS.refreshing === this.state.action) {
                    this.setState({data: result.newsList, action: STATS.refreshed});
                } else {
                    this.setState({data: result.newsList});
                }
            } else {
                this.setState({
                    data: this
                        .state
                        .data
                        .concat(result.newsList),
                    action: STATS.reset,
                    index: this.state.index - 1
                });
            }
            //console.log(result);
        } catch (err) {
            //console.log(err);
        }
    }

    handleAction = (action) => {
        if (action === this.state.action) {
            return false
        }

        if (action === STATS.refreshing) {
            this.handRefreshing();
        } else if (action === STATS.loading) {
            this.handLoadMore();
        } else {
            //DO NOT modify below code
            if (this.mount === false) {
                return;
            }
            this.setState({action: action})
        }
    }

    handRefreshing = () => {
        if (STATS.refreshing === this.state.action) {
            return false
        }

        this.update(1)
        if (this.mount === false){
            return;
        }
        this.setState({action: STATS.refreshing})
    }

    handLoadMore = () => {
        if (STATS.loading === this.state.action) {
            return false
        }
        //无更多内容则不执行后面逻辑
        if (!this.state.hasMore) {
            return;
        }

        let dataLength = this.state.data.length;
        let lastDate = this.state.data[dataLength - 1].date;
        this.update(1, lastDate)
        if (this.mount === false) {
            return;
        }

        this.setState({action: STATS.loading})
    }

    getStarImageFrom(level) {
        if (level == 1) {
            return oneStar;
        } else if (level == 2) {
            return twoStar;
        } else if (level == 3) {
            return threeStar;
        } else if (level == 4) {
            return fourStar;
        } else if (level == 5) {
            return fiveStar;
        }
    }

    renderItem() {
        let content = this.state.data.map((item, i) => {
            return(
                <li className={'cellWrapper columnDirection'}>
                    <Link to={'/detail/' + item.id} className={'cell'}>
                        <div className={'contentWrapper'}>
                            <div className={'title'}>{item.title}</div>
                        </div>
                        <img src={item.thumb} alt={''}/>
                    </Link>
                    <div className={'cellWrapper-bottom'}>
                        <div className={'dateWrapper'}>
                            <div className={'date'}>{item.date}</div>
                        </div>
                            {/* <div onClick={()=>this.chooseStar(item)} className={'starBox'}>
                                <div className={this.isSameItem(item) ? 'starBox-text purple' : 'starBox-text'}>{this.isSameItem(item) ? '取消收藏':'收藏'}</div>
                                <div className={this.isSameItem(item) ? 'starBox-iconBox purple':'starBox-iconBox'}>
                                    <Svg path={love}/>
                                </div>
                            </div> */}
                    </div>
                </li>
            );
        });
        return content;
    }

    isSameItem(item){
        let ary = JSON.parse(localStorage.getItem('news')) || [];
        let result = ary.find(((element)=>{
            return element.id === item.id;
        }))
        return result;
    }

    //todo 选择自己喜欢
    chooseStar(item) {
        let ary = JSON.parse(localStorage.getItem('news')) || [];
        //todo 先检查是否存在，如果存在就移除， 如果不存在就添加
        let hehe = ary.find(((element)=>{
            return element.id === item.id;
        }))

         if (hehe) {
            ary.splice(item, 1);
            AlertFunction({title: '提示', msg: '取消成功'})
         }else{
            ary.push(item)
            AlertFunction({title: '提示', msg: '添加成功'})
         }

        // this.setState({active: !this.state.active});
        let Ary = JSON.stringify(ary);
        localStorage.setItem('news', Ary);
        this.setState({});
    }
}