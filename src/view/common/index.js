import footer from './footer';
import header from './header';
import alert from './alert';
import loading from './loading';
import balance from './balance';
import drawer from './drawer';
import svg from './svg';

export const Footer = footer;
export const Header = header;
export const Alert = alert;
export const Loading = loading;
export const Balance = balance;
export const Drawer = drawer;
export const Svg = svg;
