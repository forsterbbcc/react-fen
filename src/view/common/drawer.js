import React, {Component} from 'react';
import arrow from '../../images/Nf-svg/back.svg';
import {Svg} from ".";
import {formatDate} from "../../lib/tool";

export default class App extends Component {
    constructor(props) {
        super(props);
        this.state = {show:false}
    }

    render() {
        return (
            <div className={'drawer'}>
                <div className={'drawer-title'} onClick={()=>this.setState({show:!this.state.show})}>
                    <div className={'left'}>
                        <div>{this.props.title}</div>
                        {
                            this.props.keys === 0 ? (
                                <div className={'point'}></div>
                            ) : ('')
                        }
                    </div>
                    <Svg path={arrow} className={'right'} status={this.state.show?'active':''}/>
                </div>
                {
                    (this.state.show)?(
                        <div className={'drawer-des'}>
                            {this.props.children}
                            {
                                this.props.time?(
                                    <p className={'time'}>{formatDate('y-m-d h:i:s',{date:this.props.time})}</p>
                                ):(null)
                            }
                        </div>
                    ):''
                }
            </div>
        )
    }

    componentWillReceiveProps({mounted}){
        if(this.props.mounted !== mounted && mounted === false){
            this.setState({
                show:false
            })
        }
    }
}