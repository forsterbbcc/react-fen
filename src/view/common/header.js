import React, {Component} from 'react';
import ReactSvg from 'react-svg';
import {Link} from 'react-router-dom';
import {Svg} from ".";

import arrow from '../../images/qh/back.svg'

export default class App extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className={'header'}>
                {
                    this.props.back ? (
                        typeof this.props.back === 'string' ? (
                            <Link to={this.props.back} className={'backTag'}>
                                <Svg path={arrow} className={'arrow'}/>
                            </Link>
                        ) : (
                            <div onClick={() => this.props.back()} className={'backTag'}>
                                <Svg path={arrow} className={'arrow'}/>
                            </div>
                        )
                    ) : (
                        <div onClick={this.props.history.goBack} className={'backTag'}>
                            <Svg path={arrow} className={'arrow'}/>
                        </div>
                    )
                }
                <div className={'title'}>{this.props.title}</div>
                {
                    this.props.right ?
                        (<Link to={{pathname:this.props.goTo,state:{simulate:this.props.simulate}}} className={'link'}>{this.props.right}</Link>) : ''
                }
                {
                    this.props.customRight ? 
                    (
                        <div className={'link'} onClick={()=>this.props.customRight.callback()}>
                            <div>{this.props.customRight.title}</div>
                        </div>
                    ):''
                }
            </div>
        );
    }
}