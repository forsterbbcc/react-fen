import React, {Component} from 'react';
import {Header} from "../common";
import {Cache} from "../../module";
import {Schedule} from "../../lib";
import {mobileMask} from "../../lib/tool";
import {Link} from 'react-router-dom';
import {Req} from "../../lib";
import AlertFunction from '../../lib/AlertFunction'

export default class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLogin: Cache.isLogin(),
            phone: '',
            code: '',
            showImg: false,
            imageAddress: '',
            imageCode: '',
            step: 3,
            count: '获取验证码',
            countNumber: 60,
            realName: '',
            id: '',
            newPass: '',
            cfmPass: ''
        };

    }

    render() {
        return (
            <div className={'changePhoneNumber'}>
                <Header title={'找回密码'} {...this.props}/>
                <div className={'main'}>
                    {
                        this.state.step === 1 ? (
                            <div>
                                <h4>找回密码</h4>
                                <ul>
                                    <li>
                                        <input type="tel" placeholder={'请输入要找回密码的手机号'} value={this.state.phone}
                                               onChange={(e) => this.setState({phone: e.target.value})} maxLength={11}/>
                                    </li>
                                    <li>
                                        <input type="tel" placeholder={'请输入短信验证码'} value={this.state.code}
                                               onChange={(e) => this.setState({code: e.target.value})} maxLength={4}/>

                                        <div className={'send'} onClick={() => this.show()}>
                                            {this.state.count}
                                        </div>
                                    </li>
                                </ul>
                                <div className={'buttonBox'}>
                                    <div className={'submit'} onClick={() => this.validPhone()}>下一步</div>
                                </div>
                            </div>
                        ) : (null)
                    }
                    {
                        this.state.step === 2 ? (
                            <div>
                                <h4>个人信息</h4>
                                <ul>
                                    <li>
                                        <span>真实姓名</span>
                                        <input type="text" placeholder={'请输入您之前登记的姓名'} value={this.state.realName}
                                               onChange={(e) => this.setState({realName: e.target.value})}
                                               maxLength={8}/>
                                    </li>
                                    <li>
                                        <span>身份证号</span>
                                        <input type="text" placeholder={'请输入您登记的身份证号'} value={this.state.id}
                                               onChange={(e) => this.setState({id: e.target.value})} maxLength={18}/>
                                    </li>
                                </ul>
                                <div className={'buttonBox'}>
                                    <div className={'submit'} onClick={() => this.validId()}>下一步</div>
                                </div>
                            </div>
                        ) : (null)
                    }
                    {
                        this.state.step === 3 ? (
                            <div>
                                <h4>重置密码</h4>
                                <ul>
                                    <li>
                                        <span>新密码</span>
                                        <input type="text" placeholder={'请输入新密码'} value={this.state.newPass}
                                               onChange={(e) => this.setState({newPass: e.target.value})}/>
                                    </li>
                                    <li>
                                        <span>确认密码</span>
                                        <input type="text" placeholder={'请再次确认密码'} value={this.state.cfmPass}
                                               onChange={(e) => this.setState({cfmPass: e.target.value})}/>
                                    </li>
                                </ul>
                                <div className={'buttonBox'}>
                                    <div className={'submit'} onClick={() => this.recoverPassword()}>确定</div>
                                </div>
                            </div>
                        ) : (null)
                    }

                    {
                        this.state.showImg ? (
                            <div className={'cover'}>
                                <div className={'codeBox'}>
                                    <p>获取验证码前,请您先填写下面数字</p>
                                    <div className={'code'}>
                                        <img src={this.state.imageAddress} alt=""/>
                                        <input type="text" placeholder={'请填写图片验证码'} value={this.state.imageCode}
                                               onChange={(e) => this.setState({imageCode: e.target.value})}/>
                                    </div>
                                    <div className={'codeButton'}>
                                        <div onClick={() => this.setState({showImg: false, imageCode: ''})}>取消</div>
                                        <div className={'sure'} onClick={() => this.confirmCode()}>确定</div>
                                    </div>
                                </div>
                            </div>
                        ) : (null)
                    }

                    <p>如遇问题，请<Link to={'/'}>联系客服</Link></p>
                </div>
            </div>
        )
    }

    componentDidMount() {
        if (Cache.initial) {
            this.loginCallback()
        } else {
            Schedule.addEventListener('cacheInitial', this.loginCallback, this)
        }
        Schedule.addEventListener('getUserInfo', this.getInfoCallback, this)
    }

    loginCallback() {
        this.setState({
            isLogin: Cache.isLogin()
        });
        this.getInfoCallback()
    }

    getInfoCallback() {
        this.setState({phone: Cache.phone})
    }


    //todo 第一步 验证手机号
    async validPhone() {
        //todo 如果手机号和验证码为空时 不能发送数据
        if (!/^1[20345789]\d{9}$/.test(this.state.phone)) return AlertFunction({title: '警告', msg: '请输入正确的手机号'});
        if (this.state.phone.length !== 0 && this.state.code.length !== 0) {
            try {
                let result = await Req({
                    url: "/api/sso/findback.htm",
                    type: 'POST',
                    data: {
                        action: 'verifyCode',
                        verifyCode: this.state.code
                    },
                    animate: true
                });
                if (result.redirectUrl === '/sso/findback.htm?step=1') {
                    this.setState({step: 2})
                } else {
                    this.setState({step: 3})
                }
            } catch (err) {
                AlertFunction({title: '错误', msg: err.errorMsg})
            }
        } else {
            AlertFunction({title: '警告', msg: '提交信息不能为空!'})
        }
    }

    //todo 第二步 验证身份信息
    validId() {
        if (this.state.realName.length !== 0 && this.state.id.length !== 0) {
            Req({
                url: "/api/sso/findback.htm",
                type: 'POST',
                data: {
                    action: 'auth',
                    name: this.state.realName,
                    identityNumber: this.state.id
                },
                animate: true
            }).then((data) => {
                AlertFunction({
                    title: '提示', msg: data.errorMsg, confirm: () => {
                        this.setState({step: 3})
                    }
                })
            }).catch((err) => {
                AlertFunction({title: '错误', msg: err.errorMsg})
            })
        } else {
            AlertFunction({title: '警告', msg: '提交信息不能为空!'})
        }
    }

    //todo 第三步 修改密码
    recoverPassword() {
        if (this.state.newPass.length !== 0 && this.state.cfmPass.length !== 0) {
            Req({
                url: "/api/sso/findback.htm",
                type: 'POST',
                data: {
                    action: 'passwd',
                    newPass: this.state.newPass,
                    newPassCfm: this.state.cfmPass
                },
                animate: true
            }).then((data) => {
                AlertFunction({
                    title: '提示', msg: data.errorMsg, confirm: () => {
                        window.history.back()
                    }
                })
            }).catch((err) => {
                AlertFunction({title: '错误', msg: err.errorMsg})
            })
        } else {
            AlertFunction({title: '警告', msg: '提交信息不能为空!'})
        }
    }


    //todo 显示图片验证码
    show() {
        if (this.state.countNumber !== 0 && this.state.count !== '获取验证码') {
            return AlertFunction({title: '警告', msg: '验证码已经发送，请稍后再试！', button: 1})
        }
        this.setState({
            showImg: true,
            imageAddress: `/api/vf/verifyCode.jpg?_=${new Date()}`
        });
    }

    //todo 确定图片验证码
    async confirmCode() {
        if (this.state.imageCode.length !== 4) {
            AlertFunction({title: '警告', msg: '请输入正确的验证码', button: 1, confirm: () => this.show()})
        } else {
            try {
                let result = await Req({
                    url: '/api/sso/findback.htm',
                    type: 'POST',
                    data: {
                        action: 'sendCode',
                        mobile: this.state.phone,
                        imageCode: this.state.imageCode
                    },
                    animate: true
                });
                if (result.success) {
                    AlertFunction({
                        title: '提示',
                        msg: result.errorMsg,
                        confirm: () => this.setState({showImg: false})
                    });
                    this.countDown()
                }
            } catch (err) {
                AlertFunction({title: '错误', msg: err.errorMsg, button: 1, confirm: () => this.show()})
            }
        }
    }

    //todo 倒计时
    countDown() {
        if (this.state.countNumber === 0) {
            this.setState({
                count: '获取验证码',
                countNumber: 60
            })
        } else {
            setTimeout(() => {
                this.setState({
                    countNumber: this.state.countNumber - 1,
                    count: `${this.state.countNumber}秒后重发`,
                });
                this.countDown()
            }, 1000)

        }
    }
}