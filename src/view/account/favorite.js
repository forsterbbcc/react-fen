import React,{Component} from 'react'
import { Header, Svg } from '../common';
import {Link} from 'react-router-dom'
import love from './../../images/xlt/love.svg';

import empty from './../../images/xlt/_empty search.svg'

export default class App extends Component{
    constructor(props){
        super(props);

        this.state = {
            data:JSON.parse(localStorage.getItem('news')) || [],
            edit:false,
            deleteIDs:[]
        }
    }

    renderEditableButton(){
        if (this.state.edit) {
            return(
                <div className={'editableButton'}>
                    <ul>
                        <li onClick={()=>this.selectAll()}>{'全选'}</li>
                        <li onClick={()=>this.removeSelectAll()}>{'删除选中'}</li>
                    </ul>
                </div>
            );
        } else {
            return ''
        }
    }

    renderItem() {
        let content = this.state.data.map((item, i) => {
            return(
                    <li  className={'cellWrapper'}>
                        <div onClick={()=>this.addDeleteID(item)} className={this.state.edit?'editBox':'hide'}>
                            <div className={this.isSelectDelete(item)?'editBox-circle-select':'editBox-circle'}></div>
                        </div>
                        <div>
                            <Link to={'/detail/' + item.id} className={'cell'}>
                                <img src={item.thumb} alt={''}/>
                                <div className={'contentWrapper'}>
                                    <div className={'title'}>{item.title}</div>
                                </div>
                            </Link>
                            <div className={'cellWrapper-bottom'}>
                                <div className={'dateWrapper'}>
                                    <div className={'date'}>{item.date}</div>
                                </div>
                                {/* <div onClick={()=>this.chooseStar(item)} className={'starBox'}>
                                    <div className={'starBox-text'}>{this.isSameItem(item) ? '取消收藏':'收藏'}</div>
                                    <div className={this.isSameItem(item) ? 'starBox-iconBox purple':'starBox-iconBox'}>
                                        <Svg path={love}/>
                                    </div>
                                </div> */}
                            </div>
                        </div>
                    </li>
            );
        });
        return content;
    }

    render(){

        if (this.state.data.length === 0) {
            return(
                <div className={'news'}>
                    <Header {...this.props} title={'我的收藏'}/>
                    <div className={'emptyBox'}>
                        <Svg path={empty}/>
                        <div>暂无数据</div>
                    </div>
                    
                </div>
            );
        }

        //样式文件是live.scss
        return(
            <div className={'news ul-content-paddingTop-offset'}>
                <Header {...this.props} title={'我的收藏'} customRight={{title:this.state.edit?'完成':'编辑',callback:()=>this.setState({edit:!this.state.edit})}}/>
                <ul className={this.state.edit?'ul-content-offset':''}>
                    {this.renderItem()}
                </ul>
                {this.renderEditableButton()}
            </div>
        );
    }

    isSelectDelete(item){
        let result = this.state.deleteIDs.find(element=>{
            return element === item.id;
        });
        return result;
    }

    addDeleteID(item){
        let result = this.state.deleteIDs.find(element=>{
            return element === item.id;
        });

        let temp = [].concat(this.state.deleteIDs);
        if (result !== undefined) {
            temp.splice(item,1);
        }else{
            temp.push(item.id);
        }

        this.setState({
            deleteIDs:temp
        });
    }

    selectAll(){
        let result = []
        for (let i = 0; i < this.state.data.length; i++) {
            result.push(this.state.data[i].id);
        }

        this.setState({
            deleteIDs:result
        });
    }

    removeSelectAll(){
        // this.setState({
        //     deleteIDs:[]
        // });
        this.removeFavorite()
    }

    removeFavorite(){

        this.setState({edit:!this.state.edit})

        if (this.state.edit === false) {
            return;
        }

        let result = [];
        for (let i = 0; i < this.state.data.length; i++) {
            const element = this.state.data[i];
            //检测是不是位于删除列表
            let tempResult = this.isSelectDelete(element);
            //如果不在删除列表中的元素则添加进新数组
            if (!tempResult) {
                result.push(element)
            }
        }
        this.setState({
            data:result,
        });
        let jsonString = JSON.stringify(result);
        localStorage.setItem('news',jsonString);
    }
}