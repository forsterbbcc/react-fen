import React,{Component} from 'react'
import AlertFunction from '../../lib/AlertFunction';
import { Header } from '../common';

export default class App extends Component{
    constructor(props){
        super(props);

        this.state = {
            mobile:'',
            email:'',
            comment:''
        }
    }

    render(){
        return(
            <div className={'accountSet'}>
                <Header title={'留言联系'} {...this.props}/>
                <div className={'commentBox'}>
                    <textarea value={this.state.comment} onChange={e=>this.setState({comment:e.currentTarget.value})} placeholder={'留言内容'} readOnly={false} cols={'30'}/>
                    <div className={'tips'}>*请填写您的联系方式</div>
                    <div className={'inputData'}>
                        <div className={'inputBox'}>
                            <span>
                                手机&nbsp;(必填)
                            </span>
                            <input value={this.state.mobile} onChange={e=>this.setState({mobile:e.currentTarget.value})} placeholder={'请填写你的手机号码'} type={'tel'}/>
                        </div>
                        <div className={'inputBox'}>
                            <span>
                                电邮 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </span>
                            <input  value={this.state.email} onChange={e=>this.setState({email:e.currentTarget.value})} placeholder={'请填写你的电邮地址'} type={'tel'}/>
                        </div>
                    </div>
                    <div onClick={()=>this.submit()} className={'submit'}>提交</div>
                </div>
            </div>
            
        );
    }

    submit(){
        if (this.state.comment.length === 0) {
            return AlertFunction({title: '提示', msg: '请输入留言'});
        }

        if (this.state.mobile.length === 0) {
            return AlertFunction({title: '提示', msg: '请输入手机号'});
        }

        if (this.state.email.length === 0) {
            return AlertFunction({title: '提示', msg: '请输入电邮'});
        }
        AlertFunction({title: '提示', msg: '提交成功'});
        this.setState({
            comment:'',
            mobile:'',
            email:''
        });
    }
}
