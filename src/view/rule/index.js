import React, {Component} from 'react';
import {Header, Drawer} from "../common";
import {Link} from 'react-router-dom';
import {Contracts, Store} from "../../module";

export default class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            rule:{}
        }
    }

    render() {
        return (
            <div className={`rulePage ${this.props.show ? 'come' : ''}`}>
                <Header title={'交易规则'} back={() => this.props.close()}/>
                <div className={'grid'}>
                    <table>
                        <tbody>
                        <tr>
                            <td>
                                交易品种
                            </td>
                            <td>{this.state.rule.name}</td>
                        </tr>
                        <tr>
                            <td>
                                货币单位
                            </td>
                            <td>{this.state.rule.currency}</td>
                        </tr>
                        <tr>
                            <td>
                                交易单位
                            </td>
                            <td>{this.state.rule.unit}</td>
                        </tr>
                        <tr>
                            <td>
                                最小波动
                            </td>
                            <td>{this.state.rule.volatility}</td>
                        </tr>
                        <tr>
                            <td>波动盈亏</td>
                            <td>{this.state.rule.volatilityIncome}</td>
                        </tr>
                        <tr>
                            <td>交易时间</td>
                            <td>
                                <p>【买入时间】</p>
                                <p dangerouslySetInnerHTML={{__html:this.state.rule.buyTimeAM}}/>
                                <p dangerouslySetInnerHTML={{__html:this.state.rule.buyTimePM}}/>
                                <p dangerouslySetInnerHTML={{__html:this.state.rule.buyTimeNI}}/>
                                <p>【卖出时间】</p>
                                <p dangerouslySetInnerHTML={{__html:this.state.rule.sellTimeAM}}/>
                                <p dangerouslySetInnerHTML={{__html:this.state.rule.sellTimePM}}/>
                                <p dangerouslySetInnerHTML={{__html:this.state.rule.sellTimeNI}}/>
                            </td>
                        </tr>
                        <tr>
                            <td>清仓时间</td>
                            <td>{this.state.rule.clearTime}</td>
                        </tr>
                        <tr>
                            <td>开仓固定点差</td>
                            <td>{this.state.rule.spread}</td>
                        </tr>
                        <tr>
                            <td>交易手续费</td>
                            <td>{this.state.rule.chargeUnit}</td>
                        </tr>
                        <tr>
                            <td>汇率</td>
                            <td>{this.state.rule.rate}</td>
                        </tr>
                        </tbody>
                    </table>
                    <div className={'tips'}>
                        <div className={'jump'}>
                            {this.state.rule.introduce}
                            <div className={'buttonGuide'}>
                                <Link to={'/newWelfare'}>一分钟了解如何交易</Link>
                            </div>
                        </div>
                    </div>

                    {/* <div className={'drawerBox'}>
                        <div>
                            <Drawer title={'新手练习'} mounted={this.props.show}>
                                <div>
                                    如果您是新手，没有交易经验，建议您到模拟练习区进行模拟交易。
                                </div>
                                <div className={'jump'} onClick={() => this.props.close()}>
                                    <Link to={{
                                        pathname: '/trade',
                                        state: {simulate: true, contract: this.props.location.state.contract}
                                    }}>进入模拟练习区</Link>
                                </div>
                            </Drawer>
                            <Drawer title={'什么是买涨？'} mounted={this.props.show}>
                                当您买涨时，价格涨了你就赚钱，跌了亏钱。
                            </Drawer>
                            <Drawer title={'什么是止盈？'} mounted={this.props.show}>
                                <p>当单笔交易盈利金额触发（多于等于）指定的止盈金额时，该笔交易会被强制平仓。</p>
                                <p>由于市场的价格实时都在变动，不保证平仓后最终盈利金额一定大于等于止盈金额，有可能会小于触发的止盈金额。</p>
                            </Drawer>
                            <Drawer title={'什么是持仓时间？'} mounted={this.props.show}>
                                <p> { this.state.rule.name }期货最后持仓时间：{ this.state.rule.clearTime }</p>
                                <p>当持仓时间到点后，持仓中的交易会被强制平仓，不保证成交价格，请务必在到期前自己选择卖出。</p>
                            </Drawer>
                            <Drawer title={'履约保证金'} mounted={this.props.show}>
                                <p>履约保证金为操盘手委托平台冻结用于履行交易亏损赔付义务的保证金。操盘手以冻结的履约保证金作为承担交易亏损赔付的上限。多出上限部分的亏损全部由合作的投资人承担。</p>
                                <p>合作交易结束后，根据清结算结果，如交易盈利，操盘手冻结的履约保证金全额退还。如交易亏损，从冻结的履约保证金中，扣减操盘手所应承担的亏损赔付额，扣减后余额退还。</p>
                            </Drawer>
                        </div>
                        <div>
                            <Drawer title={'什么是标准/迷你？'} mounted={this.props.show}>
                                选择“标准”保证金将以元为单位交易，选择“迷你”保证金和手续费将在元的基础上缩小10倍进行交易
                            </Drawer>
                            <Drawer title={'什么是买跌？'} mounted={this.props.show}>
                                当您买跌时，价格跌了你就赚钱，涨了亏钱。
                            </Drawer>
                            <Drawer title={'什么是止损？'} mounted={this.props.show}>
                                <p>当单笔交易亏损金额触发（多于等于）指定的止损金额时，该笔交易会被强制平仓。</p>
                                <p>由于市场的价格实时都在变动，不保证卖出后最终亏损金额一定小于等于止损金额，有可能会大于止损金额。</p>
                            </Drawer>
                            <Drawer title={'大涨大跌交易限制'} mounted={this.props.show}>
                                <p>交易品种涨幅≥{ this.state.rule.highLimit }时禁止买跌，跌幅≥{ this.state.rule.lowLimit }时禁止买涨。</p>
                                <p>交易品种涨幅≥{ this.state.rule.highClose }时持仓中买跌的交易全部强制平仓，跌幅≥{ this.state.rule.lowClose }时持仓中买涨的交易强制平仓。</p>
                            </Drawer>
                            <Drawer title={'盈利如何分配？'} mounted={this.props.show}>
                                <p>盈利100%归操盘手所有，投资人不参与盈利分成。</p>
                            </Drawer>
                        </div>
                    </div> */}

                    
                    
                    <Drawer title={'新手练习'} mounted={this.props.show}>
                        <div>
                            如果您是新手，没有交易经验，建议您到模拟练习区进行模拟交易。
                        </div>
                        <div className={'jump'} onClick={() => this.props.close()}>
                            <Link to={{
                                pathname: '/trade',
                                state: {simulate: true, contract: this.props.location.state.contract}
                            }}>进入模拟练习区</Link>
                        </div>
                    </Drawer>
                    <Drawer title={'什么是元模式/角模式？'} mounted={this.props.show}>
                        选择“元模式”保证金将以元为单位交易，选择“角模式”保证金和手续费将在元的基础上缩小10倍进行交易
                    </Drawer>
                    <Drawer title={'什么是买涨？'} mounted={this.props.show}>
                        当您买涨时，价格涨了你就赚钱，跌了亏钱。
                    </Drawer>
                    <Drawer title={'什么是买跌？'} mounted={this.props.show}>
                        当您买跌时，价格跌了你就赚钱，涨了亏钱。
                    </Drawer>
                    <Drawer title={'什么是止盈？'} mounted={this.props.show}>
                        <p>当单笔交易盈利金额触发（多于等于）指定的止盈金额时，该笔交易会被强制平仓。</p>
                        <p>由于市场的价格实时都在变动，不保证平仓后最终盈利金额一定大于等于止盈金额，有可能会小于触发的止盈金额。</p>
                    </Drawer>
                    <Drawer title={'什么是止损？'} mounted={this.props.show}>
                        <p>当单笔交易亏损金额触发（多于等于）指定的止损金额时，该笔交易会被强制平仓。</p>
                        <p>由于市场的价格实时都在变动，不保证卖出后最终亏损金额一定小于等于止损金额，有可能会大于止损金额。</p>
                    </Drawer>
                    <Drawer title={'什么是持仓时间？'} mounted={this.props.show}>
                        <p> { this.state.rule.name }期货最后持仓时间：{ this.state.rule.clearTime }</p>
                        <p>当持仓时间到点后，持仓中的交易会被强制平仓，不保证成交价格，请务必在到期前自己选择卖出。</p>
                    </Drawer>
                    <Drawer title={'大涨大跌交易限制'} mounted={this.props.show}>
                        <p>交易品种涨幅≥{ this.state.rule.highLimit }时禁止买跌，跌幅≥{ this.state.rule.lowLimit }时禁止买涨。</p>
                        <p>交易品种涨幅≥{ this.state.rule.highClose }时持仓中买跌的交易全部强制平仓，跌幅≥{ this.state.rule.lowClose }时持仓中买涨的交易强制平仓。</p>
                    </Drawer>
                    <Drawer title={'交易综合费'} mounted={this.props.show}>
                        <p>{ this.state.rule.name }期货每手交易综合费：{this.state.rule.chargeUnit}</p>
                        <p>（买进卖出只收取一次）</p>
                    </Drawer>
                    <Drawer title={'履约保证金'} mounted={this.props.show}>
                        <p>履约保证金为操盘手委托平台冻结用于履行交易亏损赔付义务的保证金。操盘手以冻结的履约保证金作为承担交易亏损赔付的上限。多出上限部分的亏损全部由合作的投资人承担。</p>
                        <p>合作交易结束后，根据清结算结果，如交易盈利，操盘手冻结的履约保证金全额退还。如交易亏损，从冻结的履约保证金中，扣减操盘手所应承担的亏损赔付额，扣减后余额退还。</p>
                    </Drawer>
                    <Drawer title={'盈利如何分配？'} mounted={this.props.show}>
                        <p>盈利100%归操盘手所有，投资人不参与盈利分成。</p>
                    </Drawer>

                    
                    
                    
                    
                    
                    
                    
                </div>
            </div>
        )
    }

    componentWillReceiveProps({show,location}){
        if(this.props.show !== show){
                this.mounted = show;
                if(show){
                    const code = location.state.contract.replace(/[0-9]/g,'');
                    this.setState({
                        rule:Store.rule.get()[code]
                    })
                }else{
                    this.setState({
                        rule:{}
                    })
                }
        }
    }
}