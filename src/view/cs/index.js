import React,{Component} from 'react'
import {Req, Schedule} from '../../lib'
import Header from '../common/header'
import Svg from 'react-svg'
import send from './../../images/Nf-svg/send.svg'
import cache from '../../module/cache'
import {Link} from 'react-router-dom'
import {Cache} from "../../module";

export default class App extends Component{
    mount = true;
    constructor(props){
        super(props);

        this.state = {
            content:'',
            conversation:[],
            idNumber:Cache.idNumber,
            gender:true,
            name:Cache.realName,
            isLogin:Cache.isLogin()
        }
    }

    componentDidMount() {
        if (this.state.isLogin) {
            this.loginCallback()
        } else {
            Schedule.addEventListener('cacheInitial', this.loginCallback, this);
        }
        if(Cache.idNumber){
            let num = this.state.idNumber.slice(16,17) % 2;
            num === 0 ? this.setState({gender:true}):this.setState({gender:false});
        }
    }

    componentWillUnmount(){
        this.mount = false;
        clearTimeout(this.timer);
    }

    //todo 每秒刷新后 回到最底端
    // componentDidUpdate(){
    //     this.scrollToBottom()
    // }
    //
    // scrollToBottom = () => {
    //     const messagesContainer = ReactDOM.findDOMNode(this.view);
    //     messagesContainer.scrollTop = messagesContainer.scrollHeight;
    // };

    async requestMessage () {
        try {
            let result = await Req({
                url: 'api/home/kefu.htm',
                type: 'GET',
                data: {
                    action: 'more',
                    size: 50,
                    _: new Date()
                }
            });

            if (this.mount) {
                this.setState({conversation:result.data});
            }
        } catch (err) {}

        this.timer = setTimeout(() => {
            this.requestMessage()
        }, 1000);
    }

    async sendMessage () {

        if (this.state.content === '') {
            return;
        }

        try {
            let result = await Req({
                url: 'api/home/kefu.htm',
                type: 'POST',
                data: {
                    action: 'send',
                    content:this.state.content
                }
            });
            if (this.mount){
                this.setState({content:''});
            }
        } catch (err) {
            console.log(err);
        }

        // this.timer = setTimeout(() => {
        //     this.requestMessage()
        // }, 1000);
    }


    renderItem(){
        if (this.state.isLogin) {
            let content = this.state.conversation.map((item,i)=>{
                if (item.status === 3 || item.status === 4) {
                    return(
                        <div className={'cs-cs'} key={i}>
                            {/* <div>客服:</div> */}
                            <div>
                                <div>
                                    <label>{item.content}</label>
                                </div>
                            </div>
                        </div>
                    );
                }else{
                    return(
                        <div className={'cs-user'} key={i}>
                            {/* <div className={'username'}>{Cache.realName || '用户'}</div> */}
                            <div>
                                <div>
                                    <div>
                                        <label>{item.content}</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    );
                }
            });
            return content;
        }else{
            return(
                <div className={'cs-cs'}>
                    {/*<img src={cs} alt=""/>*/}
                    <div>
                        <div>
                            <label>请先<Link to={'/login'}>登录</Link>平台账号,即可进行留言或在线咨询服务</label>
                        </div>
                    </div>
                </div>
            );
        }
    }

    render(){
        return(
            <div className={'cs'}>
                <Header title={'客服'} {...this.props}/>
                <div ref={view=>this.view = view} className={'cs-content'}>
                    {this.renderItem()}
                </div>
                <div  className={'cs-input'}>
                    <input type="text" value={this.state.content} onChange={this.onChangeText.bind(this)} placeholder={'请输入消息 ...'}/>
                    <div className={'svgWrapper'} onClick={()=>this.submit()}>发送</div>
                </div>
            </div>
        );
    }

    onChangeText(e){
        let text = e.target.value;
        if (this.mount){
            this.setState({
                content:text
            });
        }
    }

    submit(){
        this.sendMessage();
        console.log('send');
    }

    loginCallback(){
        this.setState({
            isLogin:Cache.isLogin()
        },()=>this.requestMessage())
    }
}