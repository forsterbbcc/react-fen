import {Contracts} from "../../module";
import React, {Component} from 'react';
import {Link} from 'react-router-dom';
export default class App extends Component {
    constructor(props) {
        super(props);
        this.hour = new Date().getHours()
        this.state = {
            deleteIDs:[]
        }
    }

    renderEditBox(code){
        if (this.props.edit) {
            return(
                <div onClick={()=>this.addDeleteID(code)} className={this.props.edit?'editBox':'hide'}>
                    <div className={this.isSelectDelete(code)?'editBox-circle-select':'editBox-circle'}></div>
                </div>
            );
        }else{
            return ''
        }
    }

    render() {
        return (
            <div>
                {
                    this.props.array.map((item, key) => {
                        if (item === 'empty') {
                            return '';
                        }
                        return (
                            <div className={'boxWrapper'}>
                                {this.renderEditBox(item.goodsCode)}
                                <Link to={{
                                    pathname: '/trade',
                                    state: {simulate: this.props.simulate, contract: item.code, name: item.name, goodsCode:item.goodsCode}
                                }}>
                                    {
                                        this.props.simulate ? (
                                            <div className={'box'}>
                                                <div>
                                                    <div>
                                                        {item.name}
                                                        {
                                                            Contracts.isHot(item.code) ? (
                                                                <span className={'bgRed'}>HOT</span>
                                                            ) : (null)
                                                        }
                                                        {
                                                            Contracts.isNew(item.code) ? (
                                                                <span className={'bgGreen'}>NEW</span>
                                                            ) : (null)
                                                        }
                                                    </div>
                                                    <p>{item.code}</p>
                                                </div>
                                                <div className={'change'}>
                                                    <div className={item.isUp ? 'red' : 'green'}>
                                                        {item.price || '- -'}
                                                    </div>
                                                </div>
                                                <div className={'change'}>
                                                    <div className={item.isOpen ? (item.isUp ? 'bgRed' : 'bgGreen') : 'rest'}>
                                                        {item.isOpen ? item.rate : '休市'}
                                                    </div>
                                                </div>
                                            </div>

                                        ) : ('')
                                    }
                                </Link>
                            </div>
                        )
                    })
                }
            </div>
        )
    }

    isSelectDelete(item){

        let result = this.state.deleteIDs.find(element=>{
            return element === item;
        });
        return result;
    }

    addDeleteID(item){
        let result = this.state.deleteIDs.find(element=>{
            return element === item;
        });

        let temp = [].concat(this.state.deleteIDs);
        if (result !== undefined) {
            temp.splice(item,1);
        }else{
            temp.push(item);
        }

        this.setState({
            deleteIDs:temp
        });
    }

    selectAll(){
        let data = JSON.parse(localStorage.getItem('self')) || [];
        let result = []
        for (let i = 0; i < data.length; i++) {
            result.push(data[i]);
        }

        this.setState({
            deleteIDs:result
        });
    }

    removeSelectAll(){
        this.setState({
            deleteIDs:[]
        });
    }

    removeFavorite(callback){

        // this.setState({edit:!this.state.edit})

        // if (this.props.edit) {
        //     return;
        // }

        let data = JSON.parse(localStorage.getItem('self')) || [];

        let result = [];
        for (let i = 0; i < data.length; i++) {
            const element = data[i];
            //检测是不是位于删除列表
            let tempResult = this.isSelectDelete(element);

            //如果不在删除列表中的元素则添加进新数组
            
            if (!tempResult) {
                result.push(element)
            }
        }
        // this.setState({
        //     data:result,
        // });
        let jsonString = JSON.stringify(result);
        localStorage.setItem('self',jsonString);
        callback&&callback();
    }
}