import React, {Component} from 'react';
import {Footer, Svg} from "../common";
import {Contracts} from "../../module";
import {Data} from "../../module";
import {Schedule} from "../../lib";
import Quotation from './quotation';
import tips from '../../images/tips.png';

import search from './../../images/qh/search.svg'
import back from './../../images/qh/back.svg'

export default class App extends Component {
    _mounted = false;

    constructor(props) {
        super(props);
        this.state = {
            foreignArray: [],
            stockArray: [],
            domesticArray: [],
            selfArray: [],
            allArray: [],
            cover: true,
            num: 4,//this.props.location.state.type || 1,
            edit:false,
            page: this.props.location.state && this.props.location.state.page || 'all quotation'
        };
        if (Contracts.initial) {
            this.state.foreignArray = Data.foreignBrief;
            this.state.stockArray = Data.stockBrief;
            this.state.domesticArray = Data.domesticBrief;
            this.state.allArray = Data.foreignBrief;
            this.state.allArray = this.state.allArray.concat(Data.stockBrief);
            this.state.allArray = this.state.allArray.concat(Data.domesticBrief);
            this.state.selfArray = Data.selfBrief;
            let [o] = Contracts.foreignArray;
            this.state.contract = o.contract;
            this.state.hot = Contracts.hot;
            this.state.news = Contracts.new;
            Data.start('updateBrief');
        } else {
           
        }
        Schedule.addEventListener('contractsInitial', this.updateContracts, this);
        if (localStorage.getItem('cover') !== null) {
            this.state.cover = false
        }
        // if(!!this.props.location.state){
        //     this.state.num = this.props.location.state.type
        // }
    }

    renderContent(){
        if (this.state.num === 1) {
            return(
                <div className={'main'}>
                    <Quotation array={this.state.foreignArray} simulate={this.props.location.state.simulate}
                    />
                </div>
            );
        }else if (this.state.num === 2) {
            return(
                <div className={'main'}>
                    <Quotation array={this.state.domesticArray} simulate={this.props.location.state.simulate}/>
                </div>
            );
        }else if (this.state.num === 3) {
            return(
                <div className={'main'}>
                    <Quotation array={this.state.stockArray} simulate={this.props.location.state.simulate}/>
                </div>
            );
        }else if (this.state.num === 4) {
            return(
                <div className={'main'}>
                    <Quotation array={this.state.allArray} simulate={this.props.location.state.simulate}/>
                </div>
            );
        }
    }

    renderEditButton(){
        if (this.state.num === 4) {
            return(
                <div className={'editBox'}>
                    <div onClick={()=>{

                        this.setState({edit:!this.state.edit})
                        
                    }}>{this.state.edit?'完成':'编辑'}</div>
                </div>
            );
        }else{
            return null
        }
    }

    renderFooter(){
        if (this.state.edit) {
            return (
                <div className={'editableButton'}>
                    <ul>
                        <li onClick={()=>this.selectAll()}>{'全选'}</li>
                        <li onClick={()=>this.removeSelectAll()}>{'删除选中'}</li>
                    </ul>
                </div>
            );
        }else{
            return (
                <Footer/>
            );
        }
    }

    renderPage(){
        if (this.state.page === 'all quotation') {
            return(
                <div>
                    <div className={'categoryQuotation'}>
                        <div onClick={() => {this.setState({num: 4}); Contracts.updateSelf()}}
                            className={`${this.state.num === 4 ? 'activeTop' : ''} right`}>全部
                        </div>
                        <div onClick={() => this.setState({num: 1,edit:false})}
                            className={`${this.state.num === 1 ? 'activeTop' : ''} left`}>国际期货
                        </div>
                        <div onClick={() => this.setState({num: 3,edit:false})}
                            className={`${this.state.num === 3 ? 'activeTop' : ''} middle`}>股指期货
                        </div>
                        <div onClick={() => this.setState({num: 2,edit:false})}
                            className={`${this.state.num === 2 ? 'activeTop' : ''} middle`}>国内期货
                        </div>
                    </div>
                    <div className={'quotation'}>
                        <div className={'title'}>
                            <div>品种名称</div>
                            <div>最新价</div>
                            <div>涨跌幅</div>
                        </div>
                        {this.renderContent()}
                    </div>
                </div>
            );
        }else{
            if (!!this.state.selfArray && this.state.selfArray.length === 0) {
                return(
                    <div className={'none'}>暂未设定</div>
                );
            }
            return(
                <div className={'quotation quotation-heightOffset'}>
                    <div className={'title'}>
                        <div>品种名称</div>
                        <div>最新价</div>
                        <div>涨跌幅</div>
                    </div>
                    <div className={'main'}>
                        <Quotation ref={view=>{this.quota = view}} edit={this.state.edit} array={this.state.selfArray} simulate={this.props.location.state.simulate}/>
                    </div>
                </div>
            );
        }
    }

    render() {
        return (
            <div className={'quotationBox'}>
                <div className={'headerTop'}>
                    <div onClick={()=>window.history.back()} className={'headerTop-imageBox'}>
                        <Svg path={back}/>
                    </div>
                    <div className={'pageSelection'}>
                        <div onClick={()=>this.setState({'page':'all quotation'})} className={this.state.page === 'all quotation' ? 'text selectedPage':'text'}>行情</div>
                        <div onClick={()=>this.setState({'page':'favorite'})} className={this.state.page === 'all quotation' ? 'text':'text selectedPage'}>自选</div>
                    </div>
                    <div onClick={()=>Schedule.dispatchEvent({event:'openSearch'})} className={'headerTop-imageBox'}>
                        <Svg path={search}/>
                    </div>
                </div>
                {this.renderPage()}
                {
                    this.state.cover ? (
                        <div className={'imgCover'} onClick={() => this.setState({cover: false})}>
                            <img src={tips} alt=""/>
                        </div>
                    ) : ('')
                }
            </div>
        )
    }

    componentDidMount() {
        this._mounted = true;
        Schedule.addEventListener('updateBrief', this.updateBrief, this);

        if (this._mounted) {
            setTimeout(() => {
                this.setState({
                    cover: false
                });
                localStorage.setItem('cover', 'true')
            }, 2000);
        }
    }

    componentWillUnmount() {
        this._mounted = false;
        this.setState({num:1});
        Schedule.removeEventListeners(this);
        Data.end('updateBrief');
    }

    updateContracts() {
        let [o] = Contracts.foreignArray;
        let temp = [].concat(Data.foreignBrief);
        temp = temp.concat(Data.stockBrief);
        temp = temp.concat(Data.domesticBrief);
        this.setState({
            contract: o.contract,
            foreignArray: Data.foreignBrief,
            stockArray: Data.stockBrief,
            domesticArray: Data.domesticBrief,
            selfArray: Data.selfBrief,
            allArray: temp,
            hot: Contracts.hot
        });
        Data.start('updateBrief');
    }

    updateBrief() {
        if (this._mounted) {
            let temp = [].concat(Data.foreignBrief);
            temp = temp.concat(Data.stockBrief);
            temp = temp.concat(Data.domesticBrief);
            this.setState({
                foreignArray: Data.foreignBrief,
                stockArray: Data.stockBrief,
                domesticArray: Data.domesticBrief,
                allArray: temp,
                selfArray: Data.selfBrief
            });
        }
    }

    selectAll(){
        if (this.quota) {
            this.quota.selectAll();
        }
    }

    removeSelectAll(){
        if (this.quota) {
            this.quota.removeFavorite(()=>{
                Data.end('updateBrief')
                Contracts.updateSelf();
                Data.start('updateBrief');
                this.setState({edit:false});
            })
        }
        
    }
}