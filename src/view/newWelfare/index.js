import React, {Component} from 'react';
import {Header} from "../common";
import {Link} from 'react-router-dom'
//todo 图片
import bg from '../../images/Nf-img/new.png'
export default class App extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <div className={'newWelfare'}>
                <Header title={'新手指引'} {...this.props}/>
                <div className={'mainBox'}>
                    <img src={bg} alt=""/>
                    <Link to={{pathname:'/quotation',state:{simulate:true, type:1}}}/>
                </div>
            </div>
        )
    }
}