import {Req} from './../lib/index'

export default {
    content : null,
    time : Date.now(),

    async getContent(){
        if (this.content) {

            let tempDate = Date.now();
            let cdTime = tempDate - this.time;
            if (cdTime > 3600000) {
                try {

                    this.content = await Req({
                        url:'/api/discover/index.htm'
                    })
                    this.time = Date.now()
                }catch (e) {

                }
            }

        }else{
            try {

                this.content = await Req({
                    url:'/api/discover/index.htm'
                })
                this.time = Date.now()
            }catch (e) {

            }
        }
        return new Promise((resolve,reject)=>{
            if (this.content === null) {
                reject('no content');
            } else{
                resolve(this.content);
            }
        })
    }


}