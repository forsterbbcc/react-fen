import {Req ,Schedule} from "../lib";
import AlertFunction from "../lib/AlertFunction";

export default{
    initial:false,
    // 登录为 true   未登录为 false
    status:false,
    username:'',
    userId:'',
    realBalance:0,
    gameBalance:0,
    hello:'',
    realName:'',
    phone:'',
    bankCardCount:0,
    idNumber:0,
    idNumberValid: false,
    sex:1,  //todo 1 男， 2 女
    withdrawPass:'',
    tradeQuick:false,
    tradeList:null,
    userLevel:0,
    updateUserInfo:null,
    //todo 登录初始化
    async init(){
       try{
           const isLogin = localStorage.getItem('isLogin');
           if(!isLogin)
               throw '';
           let result = await Req({
               url:'/api/trade/scheme.htm',
               data:{
                   schemeSort:0,
                   tradeType:1,
                   beginTime:'',
                   _:new Date().getTime()
               }
           });
           if(result.isLogin){
               this.status = true;
               this.getUserInfo(true);
               this.getUserScheme(result);
           }else{
               localStorage.removeItem('isLogin');
           }
       } catch (e){
           localStorage.removeItem('isLogin');
       }finally {
           this.initial = true;
           Schedule.dispatchEvent({event:'cacheInitial'})
       }
    },

    //todo 登录
    async setLogin(username, password,callback){
        try {
           let result = await Req ({
                url:'/api/sso/user_login_check',
                type:'POST',
                data:{
                    mobile:username,
                    password:password
                },
               animate:true
            });
            if(result.success){
                this.status = true;
                localStorage.setItem('isLogin',true);
                localStorage.setItem('mobile',username);
                if(callback){
                    callback()
                }else{
                    window.history.back();
                }
                this.getUserInfo(true);
                this.getUserScheme();
                Schedule.dispatchEvent({event:'loginCallback'})
            }
        } catch(err){
            AlertFunction({title:'错误',msg:err.errorMsg})
        }
    },

    //todo 获取登录状态
    isLogin(){
        return this.status
    },

    //todo 获取用户的信息
    async getUserInfo(update){
        try {
            await Req({
                url: '/api/mine/index.htm',
            }).then((data)=>{
                this.username = data.user.username;
                this.userId = data.user.id;
                this.realBalance = data.asset.money;
                this.gameBalance = data.asset.game;
                this.hello = data.hello;
            });

            await Req({
                url:'/api/mine/profile.htm'
            }).then((data)=>{
                this.bankCardCount = data.bankCardCount;
                this.realName = data.info.name;
                this.phone = data.info.mobile;
                this.idNumber = data.info.identityNumber;
                this.idNumberValid = data.info.identityNumberValid;
                this.withdrawPass = data.user.withdrawPw;
                this.userLevel = data.level;
            });
        }catch (e){

        }finally {
            Schedule.dispatchEvent({event:'getUserInfo'});
            if(update){
                this.updateUserInfo = setTimeout(()=>{
                    this.getUserInfo(true)
                },8000)
            }
        }
    },

    async getUserScheme(data){
        try{
            if(!data){
                data = await Req({
                    url:'/api/trade/scheme.htm',
                    data:{
                        schemeSort:0,
                        tradeType:1,
                        beginTime:'',
                        _:new Date().getTime()
                    }
                });
            }
            this.tradeQuick = data.tradeQuick;
            if(data.tradeList.length > 0 && this.tradeList === null){
                this.tradeList = {};
                for(let o of data.tradeList){
                    this.tradeList[o.contCode] = o;
                }
            }
            Schedule.dispatchEvent({event:'getSchemeInfo'})
        }catch (err){

        }
    },

    //todo 退出登录
    async logout(){
        try{
            await Req({
                url:'/api/sso/user_logout',
                animate:true
            }).then((data)=>{
                if(data.code === 200){
                    this.status = false;
                    window.location.href = '/';
                }
            })
        } catch(e){
            console.log(e)
        }finally{
            clearTimeout(this.updateUserInfo);
            this.updateUserInfo = null;
            localStorage.removeItem('isLogin');
            this.username = '';
            this.userId = '';
            this.realBalance = '';
            this.gameBalance = '';
            this.hello = '';
            this.bankCardCount = '';
            this.realName = '';
            this.phone = '';
            this.idNumber = '';
            this.idNumberValid = '';
            this.withdrawPass ='';
            this.tradeQuick = false;
            Schedule.dispatchEvent({event:'loginCallback'})
        }
    }
}