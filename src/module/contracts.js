import {Req, Schedule} from "../lib";
import {Contracts, Data, Quotes} from ".";
import {getSearch} from "../lib/tool";

export default {
    initial: false,
    quoteList: [],
    domestic: {},
    domesticArray: [],
    stock: {},
    stockArray: [],
    foreign: {},
    foreignArray: [],
    selfArray: [],
    total: {},
    totalArray: [],
    hot: ['CL', 'IF', 'HSI', 'DAX'],
    new: ['SC', 'NK'],
    async init() {
        try {
            const res = await Req({url: '/api', data: getSearch()});
            let {domesticCommds, foreignCommds, stockIndexCommds, contracts} = res;
            for (let e of domesticCommds) {
                this.domestic[e.code] = e;
                this.domesticArray.push(e);
            }
            for (let e of foreignCommds) {
                this.foreign[e.code] = e;
                this.foreignArray.push(e);
            }
            for (let e of stockIndexCommds) {
                this.stock[e.code] = e;
                this.stockArray.push(e);
            }
            this.totalArray = [].concat(this.domesticArray, this.foreignArray, this.stockArray);
            this.quoteList = JSON.parse(contracts);
            for (let [code, obj] of Object.entries(this.domestic)) {
                let c = this.quoteList.find((c) => {
                    return c.startsWith(code);
                });
                if (!!c) {
                    obj.contract = c;
                    this.total[c] = obj;
                }
            }
            for (let [code, obj] of Object.entries(this.foreign)) {
                let c = this.quoteList.find((c) => {
                    return c.startsWith(code);
                });
                if (!!c) {
                    obj.contract = c;
                    this.total[c] = obj;
                }
            }
            for (let [code, obj] of Object.entries(this.stock)) {
                let c = this.quoteList.find((c) => {
                    return c.startsWith(code);
                });
                if (!!c) {
                    obj.contract = c;
                    this.total[c] = obj;
                }
            }
            this.quoteList = this.totalArray.map((c) => c.contract);

            this.updateSelf(true);

            Data.init();
            Quotes.init();
            this.initial = true;
            Schedule.dispatchEvent({event: 'contractsInitial', eventData: true})
        } catch (err) {
            if (err.code !== undefined) {
                setTimeout(() => this.init(), 500)
            }
        }
    },

    isHot(contract) {
        let code = Contracts.total[contract].code;
        return this.hot.includes(code);
    },
    isNew(contract) {
        let code = Contracts.total[contract].code;
        return this.new.includes(code);
    },

    getContract(component) {
        if (this.initial) {
            let o = component.props.location.state.contract;
            console.log(component.props.location.state.contract);
            console.log(this.props);
            if (!o) {
                [o] = Contracts.foreignArray;
                o = o.contract;
                component.props.location.state.contract = o;
            }
            return o;
        } else {
            return false;
        }
    },

    updateSelf(init){
        if (localStorage.getItem('self') !== null) {
            let ary = JSON.parse(localStorage.getItem('self'));
            this.selfArray = [];
            for (let item of this.totalArray) {
                let code = item.code;
                if (ary.includes(code)) {
                    this.selfArray.push(item)
                }
            }
            
            if(!init){
                Data.updateSelf()
            }
        }
    },
}