import React from 'react';
import ReactDOM from 'react-dom';
import {Loading} from "../view/common";


const LoadingFunction = () => {
    const loading = document.createElement('div');
    document.body.appendChild(loading);

    const close = () => {
        document.body.removeChild(loading)
    };
    ReactDOM.render(
        <Loading close={close}/>,
        loading
    )
};

export default LoadingFunction
