import schedule from './schedule';
import config from '../config';
import LoadingFunction from "./loading";

export const Schedule = schedule;

export async function Req({url, type, data, ignore, animate}) {
    let body;
    let error;
    try {
        if (!!animate) LoadingFunction();
        url = url || '';

        let str = '';
        if (!!data) {
            str = '?';
            for (let [n, v] of Object.entries(data)) {
                str += `${n}=${v}&`
            }
        }
        type = type || 'GET';
        const res = await fetch(`${url}${str}`, {
            method: type,
            mode: 'cors',
            credentials: 'include',
            headers:{
                'X-Requested-With':'XMLHttpRequest'
            }
        });
        if(res.status !== 200) throw res;
        if (res._bodyText === undefined || res._bodyText.indexOf('!doctype html') === -1) {
            body = await res.text();
            body = JSON.parse(body);
        } else {
            console.warn(res);
            throw 'unknown error';
        }
    } catch (err) {
        console.warn(err);
        body = null;
        error = err;
    }

    return new Promise(function (resolve, reject) {
        if (!!animate) Schedule.dispatchEvent({event: 'loadingEnd'});
        if(ignore === true){
          resolve(body);
        } else if (body !== null) {
            if (body.code === "200" || body.code === 200 || body.status === 200 || body.code === 0 || body.errorCode === 200 || body.errorCode === 0 || body.resultCode === 200 || body.resultCode === 0) {
                if (body.success !== undefined) {
                    if (body.success) {
                        resolve(body)
                    } else {
                        reject(body)
                    }
                } else {
                    resolve(body)
                }
                
            } else {
                reject(body)
            }
        } else {
            reject(error)
        }
    })
};

export async function Jsonp({url, type, data}) {
    let quote = null;
    try {
        url = url || '';

        let str = '';
        if (!!data) {
            str = '?';
            for (let [n, v] of Object.entries(data)) {
                str += `${n}=${v}&`
            }
        }
        type = type || 'GET';
        const res = await fetch(`${config.quoteHost}${url}${str}`, {
            method: type,
            cors:true
        });
        if(res.status !== 200) throw res;
        let body = await res.text();
        body = body.match(/data:'([\s\S]+)'/);
        if (body !== null && body.length > 0) {
            [, quote] = body;
            if(quote.indexOf(';') !== -1){
                quote = quote.split(';');
                quote = quote.map((e) => e.split(','));
            }
        } else {
            quote = null;
        }
    } catch (err) {
        console.warn(err);
        quote = null;
    }
    return new Promise(function (resolve, reject) {
        if (quote) {
            resolve(quote)
        } else {
            reject(quote)
        }
    })
};