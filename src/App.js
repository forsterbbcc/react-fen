import React, {Component} from 'react';
import {BrowserRouter as Router, Switch,Route} from 'react-router-dom';
import RouteWithSubRoutes from './routes/routeWithSubRoutes';
import './lib/tool';
import routes from './routes';
import noMatch from './routes/404';
import MainMenu from './view/drawer/mainMenu'
import Search from './view/search/index'
import Share from './view/drawer/share'
import './css/index.scss';

import {Contracts,Cache} from "./module";
import {getSearch,setCookie} from "./lib/tool";

Contracts.init();
Cache.init();
document.getElementById('imgBox').style.display = 'none';

export default class App extends Component {
    constructor(props){
        super(props);
    }

    

    render() {
        return (
            <Router>
                <div>
                    <Share/>
                    <Search/>
                    <MainMenu/>
                    <Switch>
                        {
                            routes.map((route,i)=><RouteWithSubRoutes key={i} {...route} />)
                        }
                        <Route component={noMatch} />
                    </Switch>
                </div>
            </Router>
        );
    }

    componentDidMount(){
        let o = getSearch();
        if(o.ru){
            setCookie('ru',o.ru,365);
        }
    }
}
